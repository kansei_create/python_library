import numpy as np
import math
import random

"""
selectionは roulette , ranking , tournament の3つ
crossoverは　single_point , two_point , uniform の3つ
varietyは rouleteなら0 rankingbなら配列で確率を代入 tournament なら個体数
"""

def genetic(population , evaluation , variety ,crossover_rate = 0.85 , mutation_rate = 0.01, selection = "roulette" , crossover = "single_point"):

    if isinstance(variety , np.ndarray):
        if not len(variety) == len(population):
            print("配列の要素数が間違っています")
            return False
            
        if not selection == "ranking":
            print("ランキングを選んでください")
            return False
    
    if not len(population) == len(evaluation):
        print("集団の個数と評価値の個数が違います")
        return False

    for r in range(0,len(evaluation)):
        if selection == "roulette" and evaluation[r] < 0:
            print("評価値がマイナスです")
            return False

    if mutation_rate > 1 or mutation_rate < 0:
        print("確率の値として不適です")
        return  False

    if not crossover == "single_point" and not crossover == "two_point"\
      and not crossover == "uniform":
        print("crossoverの代入が間違っています")
        return False

    if not selection == "roulette" and not selection == "ranking"\
      and not selection == "tournament":
        print("selectionの代入が間違っています")
        return  False

    if selection == "roulette":
        return roulette(population , evaluation ,  crossover , crossover_rate)
    elif selection == "ranking":
        return ranking(population , evaluation ,  crossover , crossover_rate , variety)
    elif  selection == "tournament":
        return tournament(population , evaluation ,  crossover , crossover_rate , variety)
    else:
        return False


def mutation(genetic,mutation_rate):

    minimum = genetic[1][0]
    maximum = genetic[1][0] 
    
    for i in range(1,len(genetic)):
        for r in range(0,len(genetic[i])):
            
            if genetic[i][r] < minimum:
                minimum = genetic[i][r]

            if maximum < genetic[i][r]:
                maximum = genetic[i][r]
            
    for i in range(len(genetic)):
        for r in range(len(genetic[i])):
            rate = random.randint(0, 99)
            
            if rate <= mutation_rate*100:
                if isinstance(minimum,int) and isinstance(maximum,int):
                    genetic[i][r] = random.randint(minimum,maximum)
                else:
                    genetic[i][r] = random.uniform(minimum,maximum)

    return genetic

    
#単一交叉の計算
def single_point(parent):

    cut = random.randint(1, len(parent[0]))#遺伝子を切る場所を選択
    children = parent

    for i in range(0,len(parent)):#遺伝子の入れ替え
        if i < cut:
            children[0][i] = parent[1][i]
        if i >= cut:
            children[1][i] = parent[0][i]

    return children

#二点交叉の計算
def two_point(parent):

    children = parent

    while 1:
        cut1 = random.randint(1, len(parent[0]))#遺伝子を切る場所を選択
        cut2 = random.randint(1, len(parent[0]))
        
        if not cut1 == cut2:#切る場所が同じだったらだめ
            break

    if cut1 > cut2:#次の処理を楽にするために入れ替える
        cut = cut1
        cut1 = cut2
        cut2 = cut

    for i in range(0,len(parent[0])):#遺伝子の入れ替え
        if i < cut1 or i > cut2:
            children[0][i] = parent[1][i]
        if cut1 <= i <= cut2:
            children[1][i] = parent[0][i]

    return children

#一様交叉の計算
def uniform(parent):
    
    children = parent
    for i in range(len(children[0])):#遺伝子の入れ替え
        
        cut = random.randint(1, 100)#遺伝子を切るかを確率で判定

        if cut <= 50:
            children[0][i] = parent[1][i]
            children[1][i] = parent[0][i]

    return children
    
#ルーレットの選択法の計算
def roulette(population , evaluation ,  crossover , crossover_rate):
    
    rate = 0

    children = np.zeros(len(population[0]))
    
    for i in range(0,len(evaluation)):
        rate += evaluation[i]

    for i in range(0,len(evaluation)):
        evaluation[i] = evaluation[i]/rate * 100
        
        if not i == 0:
            evaluation[i] += evaluation[i-1]
    while 1:
        once = 0
        set_parent = np.array([len(evaluation),len(evaluation)])
        confilm = random.randint(0, 99)
        confilm2 = random.randint(0, 99)
        if len(children)-1 == len(population) and isinstance(children[0] ,np.ndarray):
            children = np.delete(children , 0 , 0)
            return children #子孫が一定の数できたかのチェック
        
        for i in range(0,len(evaluation)):#ルーレット法で選ぶ親のチェック
            
            if once <= confilm < evaluation[i]:
                set_parent[0] = i
                once = 0
                break
            
            once = evaluation[i]
        
        for i in range(0,len(evaluation)):#ルーレット法で選ぶ親のチェック
            if once <= confilm2 < evaluation[i]:
                set_parent[1] = i
                once = 0
                break
            
            once = evaluation[i]
            
        if set_parent[0] == set_parent[1]:#両親が同じだったら
            continue

        children_confilm = random.randint(1, 100)
        parent = np.zeros((2,len(population[0])))
        for i in range(0,len(parent[0])):#選んだ遺伝子を代入する
            parent[0][i] = population[set_parent[0]][i]
            parent[1][i] = population[set_parent[1]][i]
        
        if 0 < children_confilm  <= (crossover_rate * 100):
            
            if crossover == "single_point":
                children = np.vstack((children , single_point(parent)))
                children = mutation(children,mutation_rate)
                
            elif crossover == "two_point":
                children = np.vstack((children , two_point(parent)))
                children = mutation(children,mutation_rate)

                
            elif crossover == "uniform":
                children = np.vstack((children , uniform(parent)))
                children = mutation(children,mutation_rate)

                
def ranking_select(variety,population):
    
    chose1 = len(population)
    chose2 = len(population)

    while 1:#ランキングの確率に合わせて親を選択する
        select_rate = random.randint(1, 100)
        rate = 0
        for i in range(0,len(variety)):
            if rate <  select_rate <= variety[i]*100 + rate:
                if chose1 == len(population):
                    chose1 = i
                    break
                elif chose2 ==  len(population):
                    chose2 = i
                    break
            rate += variety[i]*100
            
        if not chose2 ==  len(population) and not chose1 == chose2:
            break
        elif  not chose2 ==  len(population) and chose1 == chose2:
            chose2 = len(population)

    return chose1 , chose2
            
def ranking(population , evaluation ,  crossover , crossover_rate , variety):
    
    rank = np.arange(len(evaluation))
    evaluation_save = evaluation
    children = np.zeros(len(population[0]))
    print(evaluation)


    for i in range(0,len(evaluation)):
        for r in range(0,len(evaluation)):#ランキングをソート
            if evaluation[i] > evaluation[r]:
                a = evaluation[r]
                evaluation[r] = evaluation[i]
                evaluation[i] = a
                b = rank[r]
                rank[r] = rank[i]
                rank[i] = b
    print(evaluation)
    print(rank)
    while 1:
        if len(children)-1 == len(population) and isinstance(children[0] ,np.ndarray):
            children = np.delete(children , 0 , 0)
            return children #子孫が一定の数できたかのチェック
        
        chose1 , chose2 = ranking_select(variety,population)
        parent = np.zeros((2,len(population[0])))
        for i in range(0,len(population[0])):
            
            parent[0][i] = population[int(rank[chose1])][i]
            parent[1][i] = population[int(rank[chose2])][i]

        children_confilm = random.randint(1, 100)
        
        if 0 < children_confilm  <= (crossover_rate * 100):
            
            if crossover == "single_point":
                children = np.vstack((children , single_point(parent)))
                children = mutation(children,mutation_rate)

            elif crossover == "two_point":
                children = np.vstack((children , two_point(parent)))
                children = mutation(children,mutation_rate)

            elif crossover == "uniform":
                children = np.vstack((children , uniform(parent)))
                children = mutation(children,mutation_rate)


def tournament_parent_select(population ,variety):
    
    select_parent = np.zeros((variety , len(population[0])))

    select_point = np.array([])

    for i in range(0,len(population)):
        
        select_point = np.append(select_point,i)

    select_point_shuffle = np.random.permutation(select_point)
    select = np.array([])

    for i in range(0,variety):
        select = np.append(select ,select_point_shuffle[i])

    parent = np.zeros((variety , len(population[0])))
    for i in range(0,len(parent)):
        for r in range(0,len(parent[0])):
            parent[i][r] = population[int(select[i])][r]
    return parent

def tournament(population , evaluation ,  crossover , crossover_rate , variety):

    children = np.zeros(len(population[0]))

    while 1:
        if len(children)-1 == len(population) and isinstance(children[0] ,np.ndarray):
            children = np.delete(children , 0 , 0)
            return children #子孫が一定の数できたかのチェック
            
        select_parent = tournament_parent_select(population ,variety);#候補の親の抽出

        number = np.array([])
        
        for i in range(0,len(select_parent)):
            number = np.append(number , i)
            
        number = np.random.permutation(number)#選んだ親をシャッフル
        parent = np.zeros((2,len(population[0])))
        print(select_parent)
        for i in range(0,len(parent[0])):#選ばれた親の抽出
            parent[0][i] = select_parent[int(number[0])][i]
            parent[1][i] = select_parent[int(number[1])][i]

        children_confilm = random.randint(1, 100)#交叉の確率を算出

        if 0 < children_confilm  <= (crossover_rate * 100):
            
            if crossover == "single_point":#各方法に合わせて子供を作る
                children = np.vstack((children , single_point(parent)))
                children = mutation(children,mutation_rate)

                
            elif crossover == "two_point":
                children = np.vstack((children , two_point(parent)))
                children = mutation(children,mutation_rate)

                
            elif crossover == "uniform":
                children = np.vstack((children , uniform(parent)))
                children = mutation(children,mutation_rate)

