import numpy as np
import random
import math

class clustering:
    #分類するデータを取得する
    def __init__(self,data):
        self.data = data

    #引数の配列の平均を求める
    def average(self,score):
        all_math = 0.0
        for i in range(0,len(score)):
            all_math += score[i]
        return all_math/len(score)

    #ピアソンの相関係数を使って、分類を行う
    def piason(self,classification):
        standard = []

        #ランダムで分類の基準となる、データを抽出する
        while 1:
            select = random.randint(0,len(self.data)-1)
            if len(standard) == 0:
                standard.append(select)
                
            elif len(standard) == classification:
                break
            
            else:
                for i in range(0,len(standard)):
                    if standard[i] == select:
                        continue
                standard.append(select)

        #選んだデータを取り出して、そのデータを元データから削除する
        select_data = self.data[standard]
        self.data = np.delete(self.data,standard,1)
        storage = []
        
        #取り出したデータを保存するためにlistにnumpyの配列を保存する
        for i in range(0,classification):
            select_list = np.array([])
            storage.append(select_list)
            
        #相関係数を求めて、データを分類する
        for i in range(0,len(self.data)):
            #相関係数を一時保存する配列を初期化する
            correlation = np.zeros(classification)

            #データの平均を求める
            average_data = self.average(self.data[i])
            
            #元のデータから一つを取り出して前に選んだ基準データとの相関係数を求める
            for r in range(0,classification):
                #変数を初期化するときに0を代入するとint型になりfloat型で使えないので0.0で初期化を行う
                df_up = 0.0
                df_right = 0.0
                df_left = 0.0
                
                #各データの平均を求める
                average_select = self.average(select_data[r])

                #相関係数の計算を行う
                for t in range(0,len(self.data[i])):
                    df_up += (self.data[i][t]-average_data)*(select_data[r][t]-average_select)
                    df_left += math.pow(self.data[i][t]-average_data,2)
                    df_right += math.pow(select_data[r][t]-average_select,2)

                #対象となる基準データとの相関係数を保存する
                correlation[r] = df_up/(math.sqrt(df_left)*math.sqrt(df_right))
            maxmizum = correlation[0]
            number = 0

            #最も相関係数の高い基準データのところに分類する
            for t in range(1,len(correlation)):
                if maxmizum < correlation[t]:
                    maxmizum = correlation[t]
                    number = t
            storage[number] = np.append(storage[number],i)

        #基準データをものに分類されたデータの平均を求めて、テキストファイルに保存する
        f = open("average.txt","w")
        for i in range(0,len(storage)):
            print(storage[i])

        for i in range(0,classification):
            average_final = np.zeros(len(self.data[0]))
            
            for r in range(0,len(storage[i])):
                average_final += self.data[int(storage[i][r])]

            average_final = average_final/len(storage[i])

            for r in range(0,len(average_final)):
                f.write(str(round(average_final[r],3)))
                f.write(" ")
            f.write("\n")
            
